/**
 * 
 * @param {string} method la méthode http à lancer (GET, POST, PUT, DELETE ...)
 * @param {string} url l'url de la ressource à requêter
 */
export function doAjax(method, url) {
    return new Promise(function (resolve, reject) {

        const ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function () {
            if (ajax.readyState === ajax.DONE) {
                if (ajax.status === 200) {

                    resolve(ajax.response);
                } else {
                    reject(ajax.status);
                }
            }
        };

        ajax.open(method, url, true);
        ajax.send();
    });
}