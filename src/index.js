//Version Callback
//import { doAjax } from "./ajax";

/**
 * On appel ici notre méthode doAjax en lui fournissant en premier la 
 * méthode http ('GET'), en deuxième argument l'url à requêter ('index.html'), 
 * en troisième argument la fonction qui ira dans la variable onSuccess
 * et qui sera exécutée dans le cas où la requête réussie, et enfin
 * en quatrième argument, la fonction qui sera exécutée dans le cas
 * où la requête a un problème.
 * On remarquera le fait que la première fonction peut avoir un argument
 * qui contiendra la réponse de la requête ajax (cet argument peut s'appeler
 * comme on le souhaite, c'est le même principe que l'argument event du
 * addEventListener)
 */
/*
doAjax('GET', 'index.html', function(reponse) {
    console.log(reponse);
}, function(status) {
    console.log('échec : '+status);
});


doAjax('GET', 'package.json', function(data) {
    console.log(JSON.parse(data));
});
*/


//Version Promise

import { doAjax } from "./ajax-promise";

doAjax('GET', 'package.json').then(function(data) {
    console.log(data);
}).catch(function(error) {
    console.log('erreur : '+error);
});