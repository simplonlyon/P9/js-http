

/**
 * 
 * @param {string} method la méthode http à lancer (GET, POST, PUT, DELETE ...)
 * @param {string} url l'url de la ressource à requêter
 * @param {function} onSuccess La fonction à exécuter en cas de succès de la requête
 * @param {function} onFailure La fonction à exécuter en cas d'échec de la requête
 */
export function doAjax(method, url, onSuccess, onFailure) {
    /*
    La classe XMLHttpRequest est l'API AJAX historique de javascript, celle
    qui sera le compatible avec le plus de navigateur.
    Elle permet de faire des requêtes HTTP directement avec javascript
    */
    const ajax = new XMLHttpRequest();
    /*
    Les requêtes HTTP faites avec Javascript ont la particularité d'être 
    "asynchrone", cela signifie que le code javascript continuera à 
    s'exécuter même avant que la requête soit terminée et qu'on lui dira
    via un event que faire avec le résultat de la requête lorsque celle ci
    sera terminée.
    Avec XMLHttpRequest, on fait ça dans l'event readystatechange qui est 
    déclenché à chaque fois que l'état de la requête est modifié
    */
    ajax.onreadystatechange = function () {
        //On lui dit de faire quelque chose seulement quand la requête est terminée
        if (ajax.readyState === ajax.DONE) {
            //On lui dit de faire un truc ou un autre selon le status http de la réponse
            if (ajax.status === 200) {
                // console.log(JSON.parse(ajax.response));
                /*
                On exécute l'argument onSuccess en partant du principe qu'il contiendra
                une fonction, et on lui donne comme valeur de son premier argument
                le résultat de la requête http
                */
                onSuccess(ajax.response);
            } else {
                // console.log('erreur http : ' + ajax.status);
                //On fait pareil avec le onFailure
                onFailure(ajax.status);
            }
        }
    };

    //On donne la méthode HTTP, l'url de ce qu'on veut requêter, et si la requête est asynchrone ou pas (elle doit toujours l'être)
    ajax.open(method, url, true);
    //On déclenche la requête avec la méthode send
    ajax.send();
}