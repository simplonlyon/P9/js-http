// const axios = require('axios');
import axios from 'axios';

axios.get('http://localhost:8000/device')
.then(function(response) {
    for(const device of response.data) {
        console.log(device.label);
    }
})
.catch(function(error) {
    console.log(error);
});

axios.post('http://localhost:8000/device', {
    label: 'from js',
    ip: '123.190.1.1',
    os: 'linux mint'
}).then(function(response) {
    console.log(response.data);

    // return axios.delete('http://localhost:8000/device/'+response.data.id);

}).catch(function(error ){
    console.log(error);
});

// axios.delete('http://localhost:8000/device/1')
// .then( () => console.log('delete'))
// .catch(error => console.log(error));

axios.get('http://localhost:8000/device/11')
.then(response => console.log(response.data))
.catch(error => console.log(error));


axios.patch('http://localhost:8000/device/11', {
    label: 'changed by js'
})
.then(response => console.log(response.data))
.catch(error => console.log(error));