module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: {
        index: './src/index.js',
        'axios-app': './src/axios-app.js'
    }
};